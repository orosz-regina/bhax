<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Stroustrup!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>JDK osztályok</title>
        <para>
            Írjunk olyan Boost C++ programot (indulj ki például a fénykardból) amely kilistázza a JDK összes
osztályát (miután kicsomagoltuk az src.zip állományt, arra ráengedve)!
        </para>
        <para>Itt az a feladatunk, hogy egy olyan c++ programot írjunk ami kiírja az adott program összes alkönyvtárjában az összes java fájlokat. Ehhez használnunk kell a Boost.FIlesystem könyvtárat.</para>
        <para>A src.zip-et innen töltöttem le:  <link xlink:href="https://github.com/fanhongtao/JDK">https://github.com/fanhongtao/JDK</link></para>
        <para>Maga a programkód nem lesz túl hosszú mivel rengeteg beépített funkciója van a boost könyvtárnak.</para>
        <para>A megoldásom forrása:  <link xlink:href="code/struotrup/java_kit.cpp"><filename>java_kit.cpp</filename></link></para>
        <para>Nézzük is a kódot.</para>
        <programlisting language="java"><![CDATA[
#include <iostream>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/xpressive/regex_actions.hpp>

using namespace std;
using namespace boost::filesystem;
int main(){
    int szamol=0;
    for (recursive_directory_iterator end, dir("./src/"); dir != end; ++dir)
    {
        if(dir->path().extension() == ".java" && is_regular_file(dir->path())){
				cout <<dir->path().filename().string() << endl;
				szamol++;
				}

    }
    cout<<endl<<count<<" java fálj van."<<endl;
}

     ]]></programlisting>
     <para>Most nézzük így egyben a kódot. Először is be kell includeolni a megfelelő könyvtárakat, ezek elengedhetetlenek ahhoz, hogy tudjuk használni a boost könyvtárat. Megadjuk neki az std, és a boost::filesystem névteret, hogy ne kelljen annyit gépelnünk. A szamol változó fogja nekünk számolni, hogy mennyi java fálj is van a könyvtárunkban.</para>
     <para>A recursive_directory_iterator azért kell, hogy ennek a segítségével járjuk be a könyvtárat, és annak alkönyvtárait (azt a könyvtárat, amit a zárójelek között megadunk). Az if-ben először megnézzük, hogy a fálj kiterjesztése az java-e, és hogy ez tényleg egy fálj-e vagy csak mappa. Végül kiiratjuk a fálj nevét és léptetjük a számlálót, amit végül megint kiiratjuk.</para>
              <figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/jdk.png" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
 </section>        

    <section>
        <title>Másoló-mozgató szemantika</title>
        <para>
            Kódcsipeteken (copy és move ctor és assign) keresztül vesd össze a C++11 másoló és a mozgató
szemantikáját, a mozgató konstruktort alapozd a mozgató értékadásra!
        </para>
        <para>A másoló és mozgató szemantika az előző félév védésének a témája volt. A megoldás teljes forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/-/blob/master/distance_learning/ziv_lempel_welch/z3a18qa_from_scratch.cpp">https://gitlab.com/nbatfai/bhax/-/blob/master/distance_learning/ziv_lempel_welch/z3a18qa_from_scratch.cpp</link></para>
               <programlisting language="java"><![CDATA[
BinTree(const BinTree & old) {
        std::cout << "BT copy ctor" << std::endl;
        
        root = cp(old.root, old.treep);
        
    }

BinTree & operator=(const BinTree & old) {
        std::cout << "BT copy assign" << std::endl;
        
        BinTree tmp{old};
        std::swap(*this, tmp);
        return *this;
    }


     ]]></programlisting>
       <para>Ez a másoló konstruktor és a másoló értékadás. Ugye az a lényege az, hogy a már meglévő fát be akarunk másolni egy üres, tmp objektumba. Először a másoló értékadás fut le, majd létrehozzuk a még üres fánkat. Ekkor hívódik meg a másoló konstruktor. Ő bemásolja az eredeti fa értékeit az ideiglenes üres fába. Ezek után az irányítás a swap függvényhez kerül, ami megcseréli a két fát. Az ideiglenesből lesz a fő fa, az eredeti meg törlődik.</para>
                  <programlisting language="java"><![CDATA[
BinTree(BinTree && old) {
        std::cout << "BT move ctor" << std::endl;
        
        root = nullptr;
        *this = std::move(old);
    }
       
    BinTree & operator=(BinTree && old) {
        std::cout << "BT move assign" << std::endl;
        
        std::swap(old.root, root);
        std::swap(old.treep, treep);
        
        return *this;
    }
]]></programlisting>
<para>A másoló szemantikával ellentétben, itt nincsenek fölösleges másolások. Másolás nélkül mozgatjuk a fát. Először az új fa gyökerét egy null pointer lesz. Majd jön a mozgató értékadás, ami megcseréli az eredeti fa gyökerére mutató pointert az újonnan létre hozott pointerrel. Így az új fa gyökere fog az fa gyökerére mutatni, az eredeti fa gyökere meg null pointer lesz. Majd pedig törölve lesz a régi fa gyökere, ami nem lesz hatással az új fára.</para>
    </section>        
    
    <section>
        <title>Hibásan implementált RSA törése</title>
        <para>
            Készítsünk betű gyakoriság alapú törést egy hibásan implementált RSA kódoló:
https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_3.pdf (71-73 fólia) által
készített titkos szövegen.
        </para>
        <para>Kicsitt bővebb információ arról, hogy mi is a RSA-eljárás. <link xlink:href="https://hu.wikipedia.org/wiki/RSA-elj%C3%A1r%C3%A1s">https://hu.wikipedia.org/wiki/RSA-elj%C3%A1r%C3%A1s</link> </para>
        <para>A megoldás: <link xlink:href="code/stroutrup/RSA.java"><filename>RSA.java</filename></link> és <link xlink:href="code/stroutrup/RSA.java"><filename>RSA.java</filename></link></para>

        <figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/RSA.png" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
    </section>        

    <section>
        <title>Változó argumentumszámú ctor</title>
        <para>
            Készítsünk olyan példát, amely egy képet tesz az alábbi projekt Perceptron osztályának bemenetére
és a Perceptron ne egy értéket, hanem egy ugyanakkora méretű „képet” adjon vissza. (Lásd még a 4
hét/Perceptron osztály feladatot is.)
        </para>
        <para>Az előző fejezetben már elkészítettük azt, hogy egy értéket adjon vissza. Most elkészítjük, hogy egy képet adjon vissza eredményül.</para>
       <para>A megoldás forrása: <link xlink:href="code/stroutrup/perceptron.cpp"><filename>perceptron.cpp</filename>.</link></para>
       <para>Nos, ez a program, az előző fejezetben látható program kiegészítése, ezért ott folytatom, ahol az abbamaradt. Megvolt a beolvasás, és az értékek vörös színkomponensének az eltárolása.</para>
                      <programlisting language="java"><![CDATA[
    for (int i = 0; i<png_image.get_width(); ++i)
        for (int j = 0; j<png_image.get_height(); ++j)
        {
            png_image[i][j].green = value[i*png_image.get_width() + j];
            png_image[i][j].green = value[i*png_image.get_height() + i];
        }
    
    png_image.write("new_mandel.png");
]]></programlisting>
<para>Megint végig megyönk a kép összes képponján, és megváltoztatjuk az adott értékeket, majd a write-al kirajzoltatjuk az új képet.</para>
        <figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/perceptron_mandel.png" scale="25" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>Ha mindent jól csinálunk akkor ezt kapjuk a futtatás után, ha bemeneti fáljnak a mandelbrot halmazt adjuk meg.</para>
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
</chapter>         
