<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Liskov helyettesítés sértése</title>
        <para>
Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a
megoldásra: jobb OO tervezés.<link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf">
https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf (93-99 fólia)</link>
(számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl. source/binom/Batfai-
Barki/madarak/)
</para>
<para>Ha egy adott T altipusa S, akkor minden olyan helyen, ahol T használható, lecserélhető T S altipusára, anélkül, hogy hatással lenne a program tulajdonságára.</para>
<programlisting language="java"><![CDATA[
class Madar
{
        
    public void  repul()
    {
            System.out.println("Repul\n");
    }
}

class Sas extends  Madar
{
    public void fly()
        {
        System.out.println("Eagle: flying..\n");
        }
}
class Pingvin extends  Madar
{
}

class Liskov
{

    public static void flyBird(Bird b)
    {
        b.fly();
    }

    public static void main(String[] args)
    {
        Madar Sas = new Sas();
        Madar Pingvin = new Pingvin();
        
        flyBird(Sas);
        flyBird(Pingvin);
        
        
    }

}
	]]></programlisting>
	<para>Liskov megsértése, mivel a pingvinek nem tudnak repülni. Az a hiba a programban, hogy feltételezi, hogy minden madár tud repülni, ami nem igaz.</para>
<figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/liskov_2.png" scale="25" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
<programlisting language="java"><![CDATA[
interface Bird
{
    
}

interface IFlyingBird extends Bird
{
    public void  fly();
}
interface INotFlyingBird extends Bird
{
}
class Eagle implements IFlyingBird
{
    public void fly()
    {
        System.out.println("Eagle: flying..\n");
    }
}
class Penguin implements INotFlyingBird
{
}

class LiskovSub
{

    public static void flyBird(IFlyingBird b)
    {
        b.fly();
    }

    public static void main(String[] args)
    {
        Eagle theEagle = new Eagle();
        Penguin  thePenguin = new Penguin();
        
        flyBird(theEagle);
        //flyBird(thePenguin); //Fordítási hiba!!
        
        
    }

} 
	]]></programlisting>
	<para>Mivel csak a repülni képes madaraknak lehetnek argomentumai, ezért nem lesz megszegve a liskov elv.</para>
    </section>        

    <section>
        <title>Szülő-gyerek</title>
        <para>Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön
keresztül csak az ős üzenetei küldhetőek!
Lásd fóliák! <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf">https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf</link> (98. fólia)</para>
        <para>A megoldás forrása: <link xlink:href="code/liskov/parent.cpp"></link></para>
        <para>
A feladar arról szól, hogy nem lehet a szülő referencián keresztül, ami egy gyerek objektumra hivatkozik, meghívni gyermeke egy olyan metódusát amit ő maga nem definiált.
        </para>
                <programlisting language="c++"><![CDATA[

#include <iostream>
#include <string>

class szulo
{
public:
        void szul_kiir()
    {
        std::cout << "Valami\n";
    }
};
class gyerek : public szulo
{
public:
        void gyerek_kiir(std::string msg)
    {
       std::cout << msg << "\n";
    }
};




   int main()
    {
        szulo* p = new szulo();
        szulo* p2 = new gyerek();
        
        std::cout << "Invoking method of parent\n";
        p->szul_kiir();
        
        std::cout << "Invoking method of child through parent ref\n";
        //p2->gyerek_kiir("This won't work");
        
        delete p;
        delete p2;
        
    }


]]></programlisting>
<para>Láthatjuk, hogy amikor ki van kommentelve az, higy a gyereken keresztül hívjuk meg a szülőt akkor gond nélkül lefut.</para>
             <figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/liskov1.png" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>Ha meg nem, akkor hiba üzenetet dob.</para>
                     <figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/liskov2.png" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
    </section>  

        <section>
        <title>Anti OO</title>
        <para>
            A BBP algoritmussal 5 a Pi hexadecimális kifejtésének a 0. pozíciótól számított 10 6, 107, 108 darab
jegyét határozzuk meg C, C++, Java és C# nyelveken és vessük össze a futási időket!
<link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066</link></para>
<para>Az előző fejezethez képest a kód megváltozott, mivel kivettük az objektum orientált tulajdonságait.</para>
        <programlisting language="java"><![CDATA[
/*
 * PiBBPBench.java
 *
 * DIGIT 2005, Javat tanítok
 * Bátfai Norbert, nbatfai@inf.unideb.hu
 *
 */
/**
 * A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
 * ezt az osztályt.
 *
 * (A PiBBP osztály a BBP (Bailey-Borwein-Plouffe) algoritmust a Pi hexa
 * jegyeinek számolását végző osztály. A könnyebb olvahatóság
 * kedvéért a változó és metódus neveket megpróbáltuk az algoritmust
 * bemutató [BBP ALGORITMUS] David H. Bailey: The BBP Algorithm for Pi.
 * cikk jelöléseihez.)
 *
 * @author Bátfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class PiBBPBench {
    /**
     * BBP algoritmus a Pi-hez, a [BBP ALGORITMUS] David H. Bailey: The
     * BBP Algorithm for Pi. alapján a {16^d Sj} részlet kiszámítása.
     *
     * @param   d   a d+1. hexa jegytől számoljuk a hexa jegyeket
     * @param   j   Sj indexe
     */
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k<=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        /* (bekapcsolva a sorozat elejen az első utáni jegyekben növeli pl.
            a pontosságot.)
        for(int k=d+1; k<=2*d; ++k)
            d16Sj += Math.pow(16.0d, d-k) / (double)(8*k + j);
         */
        
        return d16Sj - Math.floor(d16Sj);
    }
    /**
     * Bináris hatványozás mod k, a 16^n mod k kiszámítása.
     *
     * @param   n   kitevő
     * @param   k   modulus
     */
    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t <= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t < 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }
    /**
     * A [BBP ALGORITMUS] David H. Bailey: The
     * BBP Algorithm for Pi. alapján a
     * {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
     * kiszámítása, a {} a törtrészt jelöli. A Pi hexa kifejtésében a
     * d+1. hexa jegytől
     */
    public static void main(String args[]) {
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        long delta = System.currentTimeMillis();
        
        for(int d=1000000; d<1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - Math.floor(d16Pi);
            
            jegy = (int)Math.floor(16.0d*d16Pi);
            
        }
        
        System.out.println(jegy);
        delta = System.currentTimeMillis() - delta;
        System.out.println(delta/1000.0);
    }
} 
]]></programlisting>
<figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/Bench_java.png" scale="25" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
    </section>  
        <section>
        <title>Hello, Android!</title>
<para>Élesszük fel az SMNIST for Humans projektet!
https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main 
Apró módosításokat eszközölj benne, pl. színvilág.</para>
<para>Színvilág megváltoztatása:</para>
<para>Ezt a forrásban szereplő  SMNISTSurfaceView.java állományban tudjuk megtenni.</para>

<programlisting language="java"><![CDATA[
 private void cinit(android.content.Context context) {

        textPaint.setColor(android.graphics.Color.GRAY);
        textPaint.setStyle(android.graphics.Paint.Style.FILL_AND_STROKE);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(android.graphics.Paint.Align.CENTER);
        textPaint.setTextSize(50);

        msgPaint.setColor(android.graphics.Color.GRAY);
        msgPaint.setStyle(android.graphics.Paint.Style.FILL_AND_STROKE);
        msgPaint.setAntiAlias(true);
        msgPaint.setTextAlign(android.graphics.Paint.Align.LEFT);
        msgPaint.setTextSize(40);

        dotPaint.setColor(android.graphics.Color.BLACK);
        dotPaint.setStyle(android.graphics.Paint.Style.FILL_AND_STROKE);
        dotPaint.setAntiAlias(true);
        dotPaint.setTextAlign(android.graphics.Paint.Align.CENTER);
        dotPaint.setTextSize(50);

        borderPaint.setStrokeWidth(2);
        borderPaint.setColor(android.graphics.Color.GRAY);
        fillPaint.setStyle(android.graphics.Paint.Style.FILL);
        fillPaint.setColor(android.graphics.Color.YELLOW);

]]></programlisting>
<para>Ebben a kódrészletben.</para>
<itemizedlist>
    <listitem>
        <para>textPaint</para>
    </listitem>
    <listitem>
        <para>msgPaint</para>
    </listitem>
    <listitem>
        <para>dotPaint</para>
    </listitem>
    <listitem>
        <para>borderPaint</para>
    </listitem>
    <listitem>
        <para>fillPaint</para>
    </listitem>

</itemizedlist>
<para>.setColor utáni részében tehetjük meg.</para>
    </section>  
                

    <section>
        <title>Ciklomatikus komplexitás</title>
        <para>
        	Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását! Lásd a fogalom
tekintetében a <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf">https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf</link> (77-79
fóliát)!
</para>
<para>A ciklikus complexitás a metódusok minőségét jellemzi. Egy olyan szoftvermetrika ami az adptt program komplexitását határozza meg, egy konkrét számértékben kifejezve. Ez a számolás gráfelméletre alapul.</para>
<para>A prog1-es második védési program komplexitása a <link xlink:href="http://www.lizard.ws/">http://www.lizard.ws/</link> oldallal számítva.</para>
<figure>
            <title>Futási eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/komplexitas.png" scale="25" />
                </imageobject>
                <textobject>
                    <phrase>Futási eredmény</phrase>
                </textobject>
            </mediaobject>
        </figure>
    </section>                                                   
</chapter>                
