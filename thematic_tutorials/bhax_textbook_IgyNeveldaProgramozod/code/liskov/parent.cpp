#include <iostream>
#include <string>

class szulo
{
public:
        void szul_kiir()
    {
        std::cout << "Valami\n";
    }
};
class gyerek : public szulo
{
public:
        void gyerek_kiir(std::string msg)
    {
       std::cout << msg << "\n";
    }
};




   int main()
    {
        szulo* p = new szulo();
        szulo* p2 = new gyerek();
        
        std::cout << "Invoking method of parent\n";
        p->szul_kiir();
        
        std::cout << "Invoking method of child through parent ref\n";
        //p2->gyerek_kiir("This won't work");
        
        delete p;
        delete p2;
        
    }

