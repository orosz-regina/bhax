import java.util.Scanner; 
import java.io.IOException; 
import java.io.InputStreamReader; 

public class Epam {
    private int adat[];
    private int meret = 0;
    public Epam(int merete){
        if (merete < 1)
            System.out.println("Érténytelen kollekció méret!");
        else
            adat = new int[merete];
    }
    public void add(int elem) {
        //todo: Több elemet is lehessen hozzáadni!!!
        if(adat.length - meret != 0)
        {
            adat[meret] = elem;
            meret++;
        }
        else {
            System.out.println("Nincs több szabad hely a kollekcióba!");
        }
    }
    public int binaris(int kezdo, int veg, int keresett) {
        //Rendezett kollekcióra fusson!
        if(keresett>adat[veg] || keresett<adat[kezdo]){
            return -1;
        }
        if (veg >= kezdo) {
            int kozepso = kezdo + (veg - kezdo) / 2;
            if (adat[kozepso] == keresett)
                return kozepso;
            if (adat[kozepso] > keresett)
                return binaris(kezdo, kozepso - 1, keresett);
            return binaris(kozepso + 1, veg, keresett);
        }
        return -1; //-1 index nincs
    }
    public void buborek(){
        for (int i = 0; i < meret - 1; i++) {
            for (int j = 0; j < meret - i - 1; j++) {
                if (adat[j] > adat[j + 1]) {
                    int temp = adat[j];
                    adat[j] = adat[j + 1];
                    adat[j + 1] = temp;
                }
            }
        }
    }
    public void printValues(){
        for (int x: adat) {
            System.out.print(x + " ");
        }
        System.out.print(System.lineSeparator());
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in); 
        System.out.print("Adja meg a tömb méretét: ");
        int t_meret= in.nextInt();
        Epam test = new Epam(t_meret);
        System.out.print("Adjon meg a számokat: ");
        for(int i=0;i<t_meret;i++){
            int szam = in.nextInt(); 
                test.add(szam);
        }
        String ell;
        if((ell =in.nextLine()) != null){
            System.out.print("Több számot adott meg!\n");
        }
        System.out.println("Az eredeti értékek:");
        test.printValues();
        test.buborek();
        System.out.println("A rendezett értékek:");
        test.printValues();
        System.out.print("Adjon meg a keresett számot: ");
        int keresett_elem = in.nextInt();
        in.close();
        int keresett_index = test.binaris(0, test.meret-1,keresett_elem);
        if(keresett_index != -1){
            System.out.println("A keresett elem " + keresett_elem +" indexe "+ keresett_index);
        }
        else
            System.out.println("Nincs ilyen elem a kollekcióba!");
    }
}
