#include <GL/glut.h>

//Game Speed
const int FPS = 60;

int roadDivTop = 0;
int roadDivMdl = 0;
int roadDivBtm = 0;

int lrIndex = 0;
int udIndex = 0;

void startGame(){
    //Road
    glColor3f(0.412, 0.412, 0.412);
    glBegin(GL_POLYGON);
    glVertex2f(20,0);
    glVertex2f(20,100);
    glVertex2f(80,100);
    glVertex2f(80,0);
    glEnd();

    //Road Left Border
    glColor3f(1.000, 1.000, 1.000);
    glBegin(GL_POLYGON);
    glVertex2f(20,0);
    glVertex2f(20,100);
    glVertex2f(23,100);
    glVertex2f(23,0);
    glEnd();

    //Road Right Border
    glColor3f(1.000, 1.000, 1.000);
    glBegin(GL_POLYGON);
    glVertex2f(77,0);
    glVertex2f(77,100);
    glVertex2f(80,100);
    glVertex2f(80,0);
    glEnd();

    //Road Middle Border
      //TOP
    glColor3f(1.000, 1.000, 0.000);
    glBegin(GL_POLYGON);
    glVertex2f(48,roadDivTop+80);
    glVertex2f(48,roadDivTop+100);
    glVertex2f(52,roadDivTop+100);
    glVertex2f(52,roadDivTop+80);
    glEnd();
    roadDivTop--;
    if(roadDivTop<-100){
        roadDivTop =20;
    }
        //Midle
    glColor3f(1.000, 1.000, 0.000);
    glBegin(GL_POLYGON);
    glVertex2f(48,roadDivMdl+40);
    glVertex2f(48,roadDivMdl+60);
    glVertex2f(52,roadDivMdl+60);
    glVertex2f(52,roadDivMdl+40);
    glEnd();



    roadDivMdl--;
    if(roadDivMdl<-60){
        roadDivMdl =60;
    }
        //Bottom
    glColor3f(1.000, 1.000, 0.000);
    glBegin(GL_POLYGON);
    glVertex2f(48,roadDivBtm+0);
    glVertex2f(48,roadDivBtm+20);
    glVertex2f(52,roadDivBtm+20);
    glVertex2f(52,roadDivBtm+0);
    glEnd();
    roadDivBtm--;
    if(roadDivBtm<-20){
        roadDivBtm=100;
    }

    //MAIN car
        //Front Tire
    glColor3f(0.000, 0.000, 0.000);
    glBegin(GL_POLYGON);
    glVertex2f(lrIndex+24,udIndex+5);
    glVertex2f(lrIndex+24,udIndex+7);
    glVertex2f(lrIndex+32,udIndex+7);
    glVertex2f(lrIndex+32,udIndex+5);
    glEnd();
        //Back Tire
    glColor3f(0.000, 0.000, 0.000);
    glBegin(GL_POLYGON);
    glVertex2f(lrIndex+24,udIndex+1);
    glVertex2f(lrIndex+24,udIndex+3);
    glVertex2f(lrIndex+32,udIndex+3);
    glVertex2f(lrIndex+32,udIndex+1);
    glEnd();
        //Car Body
    glColor3f(0.678, 1.000, 0.184);
    glBegin(GL_POLYGON);
    glVertex2f(lrIndex+26,udIndex+1);
    glVertex2f(lrIndex+26,udIndex+8);
    glColor3f(0.000, 0.545, 0.545);
    glVertex2f(lrIndex+30,udIndex+8);
    glVertex2f(lrIndex+30,udIndex+1);
    
    glEnd();
    }

void display(){
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(0.000, 0.392, 0.000,1);
    startGame();
    glFlush();
    glutSwapBuffers();
}

void spe_key(int key, int x, int y){
        switch (key) {
        case GLUT_KEY_DOWN:
            udIndex -= 5;
            if(udIndex < -1)
                udIndex = -1;
            break;
        case GLUT_KEY_UP:
            udIndex += 5;
            if(udIndex > 90)
                udIndex = 90;
            break;

        case GLUT_KEY_LEFT:
                lrIndex -= 5;
                if(lrIndex<-1)
                    lrIndex=-1;
            break;


        case GLUT_KEY_RIGHT:
                lrIndex +=5;
                if(lrIndex>45)
                    lrIndex = 45;
            break;

        default:
                break;
        }
}

void processKeys(unsigned char key, int x, int y) {

    switch (key)
    {
        case 27:
            exit(0);
        break;

        default:
            break;
        }
}

void timer(int){
    glutPostRedisplay();
    glutTimerFunc(1000/FPS,timer,0);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(500,650);
    glutInitWindowPosition(200,20);
    glutCreateWindow("OSCI");

    glutDisplayFunc(display);
    glutSpecialFunc(spe_key);
    glutKeyboardFunc(processKeys );

    glOrtho(0,100,0,100,-1,1);
    glClearColor(0.184, 0.310, 0.310,1);

    glutTimerFunc(1000,timer,0);
    glutMainLoop();

    return 0;
}
